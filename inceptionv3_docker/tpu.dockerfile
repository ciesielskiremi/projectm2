FROM debian:buster

RUN apt update
RUN apt install curl gnupg ca-certificates zlib1g-dev libjpeg-dev unzip -y

RUN echo "deb https://packages.cloud.google.com/apt coral-edgetpu-stable main" | tee /etc/apt/sources.list.d/coral-edgetpu.list
RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

RUN apt update
RUN apt install python3 python3-pip python3-pycoral  -y

RUN curl -O https://storage.googleapis.com/download.tensorflow.org/models/tflite/mobilenet_v1_1.0_224_quant_and_labels.zip && unzip mobilenet_v1_1.0_224_quant_and_labels.zip -d /tmp && curl https://dl.google.com/coral/canned_models/mobilenet_v1_1.0_224_quant_edgetpu.tflite \
-o /tmp/mobilenet_v1_1.0_224_quant_edgetpu.tflite && curl https://raw.githubusercontent.com/tensorflow/tensorflow/master/tensorflow/lite/examples/label_image/testdata/grace_hopper.bmp -o /tmp/grace_hopper.bmp && mv /tmp/labels_mobilenet_quant_v1_224.txt /tmp/labels.txt

WORKDIR /app

# RUN pip3 install -r requirements.txt

ENTRYPOINT ["python3"]