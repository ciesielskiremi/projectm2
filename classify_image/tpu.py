# Lint as: python3
# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
import argparse
import time

from PIL import Image
from pycoral.adapters import classify
from pycoral.adapters import common
from pycoral.utils.dataset import read_label_file
from pycoral.utils.edgetpu import make_interpreter


def main():
  parser = argparse.ArgumentParser(
      formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument('-m', '--model_file', default='/tmp/mobilenet_v1_1.0_224_quant_edgetpu.tflite',
                      help='File path of .tflite file.')
  parser.add_argument('-i', '--image', default='/tmp/grace_hopper.bmp',
                      help='Image to be classified.')
  parser.add_argument('-l', '--label_file', default='/tmp/labels.txt',
                      help='File path of labels file.')
  parser.add_argument('-k', '--top_k', type=int, default=1,
                      help='Max number of classification results')
  parser.add_argument('-t', '--threshold', type=float, default=0.0,
                      help='Classification score threshold')
  parser.add_argument('-c', '--count', type=int, default=5,
                      help='Number of times to run inference')
  args = parser.parse_args()

  labels = read_label_file(args.label_file) if args.label_file else {}

  interpreter = make_interpreter(*args.model_file.split('@'))
  interpreter.allocate_tensors()

  size = common.input_size(interpreter)
  image = Image.open(args.image).convert('RGB').resize(size, Image.ANTIALIAS)
  common.set_input(interpreter, image)

  for _ in range(args.count):
    start = time.perf_counter()
    interpreter.invoke()
    inference_time = time.perf_counter() - start
    classes = classify.get_classes(interpreter, args.top_k, args.threshold)
    print('%.3fms' % (inference_time * 1000))

#   print('-------RESULTS--------')
#   for c in classes:
#     print('%s: %.3f' % (labels.get(c.id, c.id), c.score))


if __name__ == '__main__':
  main()
