import argparse
from threading import Thread
import os
import re
import paramiko
from config import config
from dictToObject import dict2obj

class ThreadWithReturnValue(Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs={}, Verbose=None):
        Thread.__init__(self, group, target, name, args, kwargs)
        self._return = None
    def run(self):
        if self._target is not None:
            self._return = self._target(*self._args,
                                                **self._kwargs)
    def join(self, *args):
        Thread.join(self, *args)
        return self._return

def avg(lst): 
    return len(lst) > 0 and sum(lst) / len(lst) or 0

def process(model, name, ip, nb_inference, images, image_sync):
    ssh = paramiko.SSHClient()
    ssh.load_system_host_keys()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ip, username="root", key_filename=config.ssh_key)
    print('%s Uploading data Start' % name)
    ftp_client=ssh.open_sftp()
    ftp_client.put('%s/%s.py' % (model, name),'/tmp/%s.py' % model)
    ftp_client.put('%s/model/%s.tflite' % (model, name),'/tmp/%s.tflite' % model)
    if not image_sync:
        for image in images:
            ftp_client.put('./image/%s' % image,'/tmp/%s' % image)
    if "docker" in model:
        ftp_client.put('%s/%s.dockerfile' % (model, name),'/tmp/%s.dockerfile' % model)
    ftp_client.close()
    print('%s Uploading data done !' % name)

    print("%s start process (%s) for %s" % (name, ip, model))
    datas = {}
    for image in images:
        datas['image'] = []
        command = 'python3 /tmp/%s.py -c %s -i /tmp/%s -m /tmp/%s.tflite' % (model, nb_inference, image, model)
        if "docker" in model:
            command = "docker build -qt %s -f /tmp/%s.dockerfile . > /dev/null && docker run --privileged -it -v /tmp:/tmp --rm %s %s" % (model, model, model, '/tmp/%s.py -c %s -i /tmp/%s -m /tmp/%s.tflite' % (model, nb_inference, image, model))
        # print(command)
        stdin, stdout, stderr = ssh.exec_command(command, get_pty=True)
        for line in iter(stdout.readline, ""):
            if re.search('.*ms', line):
                if config.log_verbose:
                    print("%s: %s" % (name, line), end="")
                datas['image'].append(float(line.split('ms')[0]))

    print("%s process done !" % (name))
    return (name,datas)

if __name__ == '__main__':
    config = dict2obj(config) # converti le dict de config en objet
    instances = list(filter(lambda x: x.to_sync == True, config.instances)) # liste des instance a traité
    images = [f for f in os.listdir('image')] # liste des images dans le repertoire image
    res = {}
    image_sync = False # flag permetant d'envoyer les imge une seul fois par instance
    for model in config.models:
        threads = []
        res[model] = {}
        for instance in instances:
            if not "docker" in model or ("docker" in model and instance.can_use_docker): #ne prend pas en compte les model docker si l'instance ne paux pas le lancer
                res[model][instance.name] = {}
                thread = ThreadWithReturnValue(target=process, args=[model, instance.name, instance.ip, config.nb_inference, images, image_sync]) #lance les thread pour process les instances en même temps
                threads.append(thread)
                thread.start()
        for thread in threads:
            datas = thread.join() # attend la fin des threads
            res[model][datas[0]]['results'] = datas[1] # récupére les résultat des thread
        image_sync = True
    print("Model %s is done !" % model)
    with open(config.export_file, 'w') as f: #export les datas
        print('instance,model,image,num_inference,temps,conso', file=f)
        for model in config.models:
            for instance in instances:
                if not "docker" in model or ("docker" in model and instance.can_use_docker):
                    print("time moy for %s on %s : %.3fms on %s inference for %s image (%s)" % (instance.name, model, avg([avg(res[model][instance.name]['results'][image]) for image in res[model][instance.name]['results']]), config.nb_inference, len(images), config.nb_inference * len(images)))
                    for image in images:
                        for i, temps in enumerate(res[model][instance.name]['results']['image']):
                            conso = instance.power_use * (temps / 3600000)
                            print('%s,%s,%s,%s,%s,%.16f' % (instance.name, model, image, i + 1, temps, conso), file=f)