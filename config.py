config = {
    'instances': [
        {
            'name' : 'rasp',
            # IP For Rapsberry without TouchScreen
            #'ip' : '192.168.1.29',
            # IP For Rapsberry with TouchScreen
            'ip' : '192.168.0.29',
            # Power consumption for Rapsberry without TouchScreen
            #'power_use' : 6.4,
            # Power consumption for Rapsberry with TouchScreen
            'power_use' : 9.2,
            'to_sync' : True,
            'can_use_docker': True
        },
        {
            'name' : 'tpu',
            'ip' : '192.168.1.40',
            'power_use' : 7.7,
            'to_sync' : False,
            'can_use_docker': True
        },
        {
            'name' : 'gpu',
            'ip' : '192.168.0.27',
            'power_use' : 12,
            'to_sync' : False,
            'can_use_docker': True
        }
    ],
    'nb_inference': 10,
    'models': [
        'mobilenetv1', 
        # 'mobilenetv1_docker',
        # 'inceptionv3',
        # 'inceptionv3_docker',
        # 'ssd_mobilenet',
        # 'ssd_mobilenet_docker',
        ],
    'log_verbose': False,
    'export_file': 'export.csv',
    'ssh_key': '../../../.ssh/gitkraken_rsa'
}